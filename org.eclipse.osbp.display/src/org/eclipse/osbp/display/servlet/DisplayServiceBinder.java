/**
 * Copyright (c) 2011 - 2017, Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 		Loetz GmbH&Co.KG - Initial implementation
 */
package org.eclipse.osbp.display.servlet;

import org.eclipse.osbp.core.api.persistence.IPersistenceService;
import org.eclipse.osbp.runtime.common.event.IEventDispatcher;
import org.eclipse.osbp.ui.api.metadata.IDSLMetadataService;
import org.eclipse.osbp.ui.api.report.IReportProvider;
import org.eclipse.osbp.ui.api.themes.IThemeResourceService;
import org.eclipse.osbp.ui.api.useraccess.IUserAccessService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.slf4j.Logger;

@Component
public class DisplayServiceBinder {
  private static Logger log = org.slf4j.LoggerFactory.getLogger("servicebinder");
  
  private static IPersistenceService persistenceService;
  
  private static IThemeResourceService themeResourceService;
  
  private static IUserAccessService userAccessService;
  
  private static IDSLMetadataService dslMetadataService;
  
  private static IReportProvider reportProvider;
  
  private static IEventDispatcher eventDispatcher;
  
  public static IPersistenceService getPersistenceService() {
    return persistenceService;
  }
  
  public static IThemeResourceService getThemeResourceService() {
    return themeResourceService;
  }
  
  public static IUserAccessService getUserAccessService() {
    return userAccessService;
  }
  
  public static IDSLMetadataService getDSLMetadataService() {
    return dslMetadataService;
  }
  
  public static IReportProvider getReportProvider() {
    return reportProvider;
  }
  
  public static IEventDispatcher getEventDispatcher() {
	  return eventDispatcher;
  }
  
  @Reference(cardinality = ReferenceCardinality.MANDATORY, policy = ReferencePolicy.STATIC)
  public synchronized void bindPersistenceService(final IPersistenceService persistenceService) {
    DisplayServiceBinder.persistenceService = persistenceService;
    log.debug("Display PersistenceService bound");
  }
  
  public synchronized void unbindPersistenceService(final IPersistenceService persistenceService) {
    DisplayServiceBinder.persistenceService = null;
    log.debug("Display PersistenceService unbound");
  }
  
  @Reference(cardinality = ReferenceCardinality.MANDATORY, policy = ReferencePolicy.STATIC)
  public synchronized void bindThemeResourceService(final IThemeResourceService themeResourceService) {
    DisplayServiceBinder.themeResourceService = themeResourceService;
    log.debug("Display ThemeResourceService bound");
  }
  
  public synchronized void unbindThemeResourceService(final IThemeResourceService themeResourceService) {
    DisplayServiceBinder.themeResourceService = null;
    log.debug("Display ThemeResourceService unbound");
  }
  
  @Reference(cardinality = ReferenceCardinality.MANDATORY, policy = ReferencePolicy.STATIC)
  public synchronized void bindUserAccessService(final IUserAccessService userAccessService) {
    DisplayServiceBinder.userAccessService = userAccessService;
    log.debug("Display UserAccessService bound");
  }
  
  public synchronized void unbindUserAccessService(final IUserAccessService userAccessService) {
    DisplayServiceBinder.userAccessService = null;
    log.debug("Display UserAccessService unbound");
  }
  
  @Reference(cardinality = ReferenceCardinality.MANDATORY, policy = ReferencePolicy.STATIC)
  public synchronized void bindDSLMetadataService(final IDSLMetadataService dslMetadataService) {
    DisplayServiceBinder.dslMetadataService = dslMetadataService;
    log.debug("Display DSLMetadataService bound");
  }
  
  public synchronized void unbindDSLMetadataService(final IDSLMetadataService dslMetadataService) {
    DisplayServiceBinder.dslMetadataService = null;
    log.debug("Display DSLMetadataService unbound");
  }
  
  @Reference(cardinality = ReferenceCardinality.MANDATORY, policy = ReferencePolicy.STATIC)
  public synchronized void bindReportProvider(final IReportProvider reportProvider) {
    DisplayServiceBinder.reportProvider = reportProvider;
    log.debug("Display ReportProvider bound");
  }
  
  public synchronized void unbindReportProvider(final IReportProvider reportProvider) {
    DisplayServiceBinder.reportProvider = null;
    log.debug("Display ReportProvider unbound");
  }

  @Reference(cardinality = ReferenceCardinality.MANDATORY, policy = ReferencePolicy.STATIC)
  public synchronized void bindEventDispatcher(final IEventDispatcher eventDispatcher) {
    DisplayServiceBinder.eventDispatcher = eventDispatcher;
    log.debug("Display EventDispatcher bound");
  }
  
  public synchronized void unbindEventDispatcher(final IEventDispatcher eventDispatcher) {
    DisplayServiceBinder.eventDispatcher = null;
    log.debug("Display EventDispatcher unbound");
  }
}
