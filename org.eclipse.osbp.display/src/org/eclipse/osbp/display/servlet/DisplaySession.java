/**
 * Copyright (c) 2011 - 2017, Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 		Loetz GmbH&Co.KG - Initial implementation
 */package org.eclipse.osbp.display.servlet;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.osbp.runtime.common.session.AbstractSession;
import org.eclipse.osbp.runtime.common.session.ISession;
import org.eclipse.osbp.runtime.common.session.ISession.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.ui.UI;

/**
 * This session is registered as an OSGi service any time a new UI-Instance is
 * created.
 */
public class DisplaySession extends AbstractSession {

	private static Logger LOGGER = LoggerFactory.getLogger(DisplaySession.class);

	final DisplayUI ui;

	final List<ISession> slaves = new ArrayList<>();
	private ISession masterSession;
	private final String fragment;

	public DisplaySession(DisplayUI ui, IEclipseContext eclipseContext, String fragment) {
		super(eclipseContext);
		this.ui = ui;
		this.fragment = fragment;
		LOGGER.error("*** display fragment:{} ***", fragment);
		setType(Type.SLAVE);
	}

	@Override
	public <T> T get(Class<T> key) {
		return eclipseContext.get(key);
	}

	@Override
	public Object get(String key) {
		return eclipseContext.get(key);
	}

	@Override
	protected <T> CompletableFuture<T> doAsync(final Function<ISession, T> function, ExecutorService executor) {
		CompletableFuture<T> promise = new CompletableFuture<>();
		runVaadinAsync(function, promise);
		return promise;
	}

	protected <T> void runVaadinAsync(final Function<ISession, T> function, CompletableFuture<T> promise) {
		ui.access(() -> {
			try {
				T value = function.apply(this);
				// notify the promise
				promise.complete(value);
			} catch (Exception e) {
				promise.completeExceptionally(e);
			}
		});
	}

	@Override
	public void set(String key, Object object) {
		eclipseContext.set(key, object);
	}

	@Override
	public List<ISession> getSlaves() {
		return Collections.unmodifiableList(slaves);
	}

	@Override
	public List<ISession> getSlaves(Predicate<ISession> filter) {
		return slaves.stream().filter(filter).collect(Collectors.toList());
	}

	@Override
	public void addSlave(ISession slave) {
		if (isSlaveSession()) {
			throw new IllegalStateException("Not allowed for slaves");
		}
		slaves.add(slave);
	}

	@Override
	public void removeSlave(ISession slave) {
		if (isSlaveSession()) {
			throw new IllegalStateException("Not allowed for slaves");
		}
		slaves.remove(slave);
	}

	@Override
	public boolean isMasterSession() {
		return getType() == Type.MASTER;
	}

	@Override
	public boolean isSlaveSession() {
		return getType() == Type.SLAVE;
	}

	@Override
	public ISession getMaster() {
		return masterSession;
	}

	@Override
	public String getHost() {
		try {
			InetAddress netAddress = InetAddress
					.getByName(eclipseContext.get(UI.class).getPage().getWebBrowser().getAddress());
			return netAddress.getHostName();
		} catch (UnknownHostException e) {
			LOGGER.error("{}", e);
			throw new IllegalArgumentException(e);
		}
	}

	@Override
	public String getFragment() {
		return fragment;
	}

	@Override
	public void sendData(Map<String, Object> data) {
		ui.sendData(data);
	}

	@Override
	public Object getUI() {
		return this.ui;
	}

}
