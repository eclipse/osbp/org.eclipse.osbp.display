/**
 * Copyright (c) 2011 - 2017, Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 		Loetz GmbH&Co.KG - Initial implementation
 */
package org.eclipse.osbp.display.servlet;

import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.DeploymentConfiguration;
import com.vaadin.server.ServiceException;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinServletService;

/**
 * An implementation of VaadinServlet that uses SimpleUI as its base UI.
 */
@SuppressWarnings("serial")
@VaadinServletConfiguration(ui = DisplayUI.class, widgetset = "org.eclipse.osbp.vaadin.widgetset.OSBPWidgetset", productionMode = false)
public class DisplayVaadinServlet extends VaadinServlet {

	@Override
	protected VaadinServletService createServletService(
			DeploymentConfiguration deploymentConfiguration)
			throws ServiceException {
		// see http://dev.vaadin.com/ticket/15516
		DisplayServletService service = new DisplayServletService(this,
				deploymentConfiguration);
		service.init();
		return service;
	}

}
