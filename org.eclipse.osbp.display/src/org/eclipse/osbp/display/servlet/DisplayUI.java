/**
 * Copyright (c) 2011 - 2017, Lunifera GmbH (Gross Enzersdorf), Loetz GmbH&Co.KG (Heidelberg)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 		Loetz GmbH&Co.KG - Initial implementation
 */
package org.eclipse.osbp.display.servlet;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Pattern;

import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.RegistryFactory;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.EclipseContextFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.services.translation.TranslationService;
import org.eclipse.e4.ui.di.UISynchronize;
import org.eclipse.e4.ui.internal.workbench.WorkbenchLogger;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.MApplicationFactory;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.osbp.display.api.IDisplayUiParticipant;
import org.eclipse.osbp.display.api.IDisplayUiParticipantHandle;
import org.eclipse.osbp.runtime.common.session.ISession;
import org.eclipse.osbp.runtime.common.session.SessionUtil;
import org.eclipse.osbp.runtime.common.session.SessionUtil.Info;
import org.eclipse.osbp.runtime.web.vaadin.databinding.VaadinObservables;
import org.eclipse.osbp.ui.api.metadata.IDSLMetadataService;
import org.eclipse.osbp.ui.api.themes.IThemeResourceService;
import org.eclipse.osbp.vaaclipse.addons.softwarefactory.service.OSBPTranslationService;
import org.eclipse.osbp.vaaclipse.publicapi.events.IWidgetModelAssociations;
import org.eclipse.osbp.vaadin.emf.api.IModelingContext;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.annotations.PreserveOnRefresh;
import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

/**
 * The Class DisplayUI controls all mobile fragments.
 */
@SuppressWarnings({ "serial", "restriction" })
@Theme("osbp")
@Widgetset("org.eclipse.osbp.vaadin.widgetset.widgetsets.OSBPWidgetset")
@PreserveOnRefresh
@Title("Vaadin Display Preview")
@Push
public class DisplayUI extends UI {

	private static final String OS_BEE_DISPLAY = "OS.bee - Display";

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(DisplayUI.class);

	/** The ui handle. */
	private transient IDisplayUiParticipantHandle uiHandle;

	private transient IEclipseContext appContext;

	private transient Info info;

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.vaadin.ui.UI#init(com.vaadin.server.VaadinRequest)
	 */
	@Override
	protected void init(VaadinRequest request) {
		// fragment has pattern #{master-host}?{display=displayName}&{locale=languageTag} NOSONAR
		String fragment = getPage().getUriFragment();
		
		info = SessionUtil.getFragmentInfo(fragment);
		createEnvironment();

		LOGGER.error("*** init display master:{} display:{} locale:{} ***", info.host, info.ui, info.languageTag);
		setTheme("osbp");

		VaadinObservables.getRealm(getUI());

		VerticalLayout layout = new VerticalLayout();
		layout.setSizeFull();
		setContent(layout);

		// initialize the UI
		ServiceReference<IDisplayUiParticipant> reference = Activator.getServiceReference(info.ui);
		if (reference != null) {
			IDisplayUiParticipant participant = Activator.getContext().getService(reference);
			if (uiHandle != null) {
				try {
					uiHandle.dispose();
				} finally {
					uiHandle = null;
				}
			}
			uiHandle = participant.createHandle(this, info.ui);
			ContextInjectionFactory.inject(uiHandle, appContext);
			if (uiHandle != null) {
				uiHandle.handle(layout, appContext);
			}
		}
	}

	private void createEnvironment() {
		prepareEnvironment();
		registerSessionService();
	}

	/**
	 * Registers the session service.
	 * 
	 * @param request
	 */
	private void registerSessionService() {
		BundleContext bc = FrameworkUtil.getBundle(getClass()).getBundleContext();

		Dictionary<String, Object> dictionary = new Hashtable<>(); // NOSONAR
		dictionary.put("vaadinUIId", Integer.toString(getUIId()));
		dictionary.put("masterId", info.host);

		try {
			InetAddress netAddress = InetAddress.getByName(getPage().getWebBrowser().getAddress());
			String hostName = netAddress.getHostName().toLowerCase();
			LOGGER.error("*** display hostname:{} ***", hostName);
			appContext.set("ip", hostName);
		} catch (UnknownHostException e) {
			LOGGER.error("{}", e);
		}

		appContext.set(ISession.Type.class, ISession.Type.SLAVE);

		DisplaySession session = new DisplaySession(this, appContext, getPage().getUriFragment());
		appContext.set(ISession.class, session);

		bc.registerService(ISession.class, session, dictionary);
	}

	public void prepareEnvironment() {
		appContext = createDefaultContext();
		appContext.set("e4ApplicationInstanceId", UUID.randomUUID().toString());
		appContext.set("vaadinUI", this);
		appContext.set(UI.class, this);
		appContext.set("displayName", info.ui);
		appContext.set(ISession.Type.class, ISession.Type.SLAVE);
		appContext.set(UISynchronize.class, new UISynchronize() {
			public void syncExec(Runnable runnable) {
				VaadinObservables.activateRealm(DisplayUI.this);
				DisplayUI.this.accessSynchronously(runnable);
			}

			public void asyncExec(Runnable runnable) {
				VaadinObservables.activateRealm(DisplayUI.this);
				DisplayUI.this.access(runnable);
			}
		});

	}

	/**
	 * Creates the default context.
	 *
	 * @param applicationContext
	 *            the application context
	 * @return the i eclipse context
	 */
	public IEclipseContext createDefaultContext() {
		IEclipseContext serviceContext = EclipseContextFactory
				.createServiceContext(FrameworkUtil.getBundle(getClass()).getBundleContext());
		final IEclipseContext displayContext = serviceContext.createChild("DisplayContext"); //$NON-NLS-1$
		displayContext.set(org.eclipse.e4.core.services.log.Logger.class,
				new WorkbenchLogger("org.eclipse.osbp.display"));
		displayContext.set(UI.class, this);

		MApplication dummyApplication = MApplicationFactory.INSTANCE.createApplication();
		dummyApplication.setContext(displayContext);
		dummyApplication.setElementId("dummy Application");
		displayContext.set(MApplication.class, dummyApplication);

		// translation
		serviceContext.set(TranslationService.LOCALE, Locale.forLanguageTag(info.languageTag));
		serviceContext.set(TranslationService.class, ContextInjectionFactory.make(OSBPTranslationService.class, serviceContext));
		displayContext.set(IExtensionRegistry.class.getName(), RegistryFactory.getRegistry());

		displayContext.set(IWidgetModelAssociations.class, new DummyIWidgetModelAssociations());
		displayContext.set(IModelingContext.class, new DummyIModelingContext());
		displayContext.set(IDSLMetadataService.class, DisplayServiceBinder.getDSLMetadataService());
		displayContext.set(IThemeResourceService.class, DisplayServiceBinder.getThemeResourceService());

		return displayContext;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.vaadin.ui.UI#detach()
	 */
	@Override
	public void detach() {
		LOGGER.debug("detach");
		super.detach();

		if (uiHandle != null) {
			LOGGER.debug("dispose handle");
			uiHandle.dispose();
			uiHandle = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.vaadin.ui.UI#attach()
	 */
	@Override
	public void attach() {
		LOGGER.debug("attach");
		super.attach();
	}

	/**
	 * Gets the title text.
	 *
	 * @return the title text
	 */
	protected String getTitleText() {
		return OS_BEE_DISPLAY;
	}

	public void sendData(Map<String, Object> data) {
		if (uiHandle != null) {
			uiHandle.sendData(data);
		}
	}

	static class DummyIWidgetModelAssociations implements IWidgetModelAssociations {

		@Override
		public boolean addThirdParty(IWidgetModelAssociations e) {
			return false;
		}

		@Override
		public boolean removeThirdParty(IWidgetModelAssociations o) {
			return false;
		}

		@Override
		public Object getWidget(EObject element) {
			return null;
		}

		@Override
		public EObject getElement(Object component) {
			return null;
		}

	}

	static class DummyIModelingContext implements IModelingContext {

		@Override
		public EObject getModelForUi(Object component) {
			return null;
		}

		@Override
		public void loadPerspective(URI uri) {
			// not needed here
		}

		@Override
		public void setPerspective(URI uri) {
			// not needed here
		}

		@Override
		public AdapterFactory getAdapterFactory() {
			return null;
		}

		@Override
		public CommandStack getCommandStack() {
			return null;
		}

		@Override
		public boolean isDesignMode() {
			return false;
		}

		@Override
		public void setDesignMode(boolean value) {
			// not needed here
		}

		@Override
		public EditingDomain getEditingDomain() {
			return null;
		}
	}
}
